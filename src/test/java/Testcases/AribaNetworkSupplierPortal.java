package Testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.AribaHome;
import ObjectRepositories.AribaLogin;

public class AribaNetworkSupplierPortal {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://service.ariba.com/Supplier.aw/125017037/aw?awh=r&awssk=jEwiZasL&dard=1");

	}
	@Test
	public void AribaNetworkSupplier() throws InterruptedException
	{
		AribaLogin aLogin=new AribaLogin(driver);
		aLogin.WaitFunction();
		aLogin.Username().sendKeys("admin@netlink-group.com");
		aLogin.Password().sendKeys("Venus2009!");
		aLogin.Login().click();
		AribaHome aHome=new AribaHome(driver);
		aHome.ViewEnablementTasks();
		//aHome.ViewMessages();
		aHome.Logout();
	}

}
