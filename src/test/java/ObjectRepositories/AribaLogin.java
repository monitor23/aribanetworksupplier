package ObjectRepositories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AribaLogin {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public AribaLogin(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 180);
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = ".w-txt-dsize")
	WebElement username;
	@FindBy(id = "Password")
	WebElement password;
	@FindBy(css = ".w-login-page-form-btn")
	WebElement login;

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() {
		return login;
	}
}
