package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AribaHome {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public AribaHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 180);
		PageFactory.initElements(driver, this);
	}

	public void ViewEnablementTasks() throws InterruptedException {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("_xne7gb"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#SESupplierTasks > div:nth-child(1)"))).click();
		Thread.sleep(6000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("_p02gl"))).click();
		Thread.sleep(6000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("_n9izyb"))).click();

	}

	public void ViewMessages() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("_co1ps"))).click();
		Thread.sleep(2000);

	}

	public void Logout() throws InterruptedException {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".fd-identifier"))).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("lout"))).click();
		Thread.sleep(3000);
		driver.manage().deleteAllCookies();
		driver.quit();
	}

}
